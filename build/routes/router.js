"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const room_1 = __importDefault(require("./room"));
const producer_1 = __importDefault(require("./producer"));
const consumer_1 = __importDefault(require("./consumer"));
const mute_1 = __importDefault(require("./mute"));
exports.default = () => {
    const router = (0, express_1.Router)({ mergeParams: true });
    router.use('/room', room_1.default);
    router.use('/producer', producer_1.default);
    router.use('/consumer', consumer_1.default);
    router.use('/mute', mute_1.default);
    return router;
};
