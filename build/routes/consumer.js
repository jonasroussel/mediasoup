"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mediasoup_1 = require("~/tools/mediasoup");
const route = (0, express_1.Router)();
route.post('/add', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const transport = yield req.room.router.createWebRtcTransport({
        listenIps: [{ ip: process.env.HOST }],
        enableTcp: true,
        enableUdp: true,
        preferUdp: true,
    });
    req.user.consumers[req.body.producer_id] = new mediasoup_1.Consumer(transport);
    return res.status(200).json({
        transport: {
            id: transport.id,
            ice_parameters: transport.iceParameters,
            ice_candidates: transport.iceCandidates,
            dtls_parameters: transport.dtlsParameters,
        },
    });
}));
route.post('/connect', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    req.user.consumers[req.body.producer_id].transport.connect({ dtlsParameters: req.body.dtls_parameters });
    return res.status(200).json({});
}));
route.post('/consume', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const transport = req.user.consumers[req.body.producer_id].transport;
    const consumer = yield transport.consume({
        producerId: req.body.producer_id,
        rtpCapabilities: req.body.rtp_capabilities,
    });
    req.user.consumers[req.body.producer_id].consumer = consumer;
    return res.status(200).json({
        consumer_id: consumer.id,
        producer_id: consumer.producerId,
        kind: consumer.kind,
        rtp_parameters: consumer.rtpParameters,
    });
}));
exports.default = route;
