"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mediasoup_1 = require("~/tools/mediasoup");
const route = (0, express_1.Router)();
route.post('/join', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (req.user != null)
        return res.status(400).json({});
    const users = Object.values(req.room.users);
    req.room.addUser(new mediasoup_1.User(req.header('x-client-username')));
    return res.status(200).json({
        rtp_capabilities: req.room.router.rtpCapabilities,
        users: users.map((u) => u.toJSON()),
    });
}));
exports.default = route;
