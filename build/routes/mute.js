"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const route = (0, express_1.Router)();
route.post('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const mute = req.body.mute || false;
    if (req.user.producer.producer.id === req.body.producer_id) {
        const producer = req.user.producer;
        if (mute) {
            yield producer.mute();
        }
        else {
            yield producer.unmute();
        }
    }
    else {
        const consumer = req.user.consumers[req.body.producer_id];
        if (mute) {
            yield consumer.mute();
        }
        else {
            yield consumer.unmute();
        }
    }
    return res.status(200).json({});
}));
exports.default = route;
