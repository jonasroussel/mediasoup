"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const main_1 = require("~/main");
const mediasoup_1 = require("~/tools/mediasoup");
const route = (0, express_1.Router)();
route.post('/add', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const transport = yield req.room.router.createWebRtcTransport({
        listenIps: [{ ip: process.env.HOST }],
        enableTcp: true,
        enableUdp: true,
        preferUdp: true,
    });
    req.user.producer = new mediasoup_1.Producer(transport);
    return res.status(200).json({
        transport: {
            id: transport.id,
            ice_parameters: transport.iceParameters,
            ice_candidates: transport.iceCandidates,
            dtls_parameters: transport.dtlsParameters,
        },
    });
}));
route.post('/connect', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    yield req.user.producer.transport.connect({ dtlsParameters: req.body.dtls_parameters });
    return res.status(200).json({});
}));
route.post('/produce', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const producer = yield req.user.producer.transport.produce({
        kind: req.body.kind,
        rtpParameters: req.body.rtp_parameters,
    });
    req.user.producer.producer = producer;
    main_1.wsApp.emit(`${req.room.id}-user-join`, {
        username: req.user.username,
        producer_id: producer.id,
    });
    return res.status(200).json({
        producer_id: producer.id,
    });
}));
exports.default = route;
