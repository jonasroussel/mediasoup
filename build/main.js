"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.wsApp = void 0;
require("module-alias/register");
require('dotenv').config();
require("~/tools/logger");
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const cors_1 = __importDefault(require("cors"));
const https_1 = __importDefault(require("https"));
const socket_io_1 = __importDefault(require("socket.io"));
const express_1 = __importDefault(require("express"));
const router_1 = __importDefault(require("~/routes/router"));
const mediasoup_1 = __importDefault(require("./tools/mediasoup"));
const errorHandlers_1 = __importDefault(require("~/tools/errorHandlers"));
const PORT = parseInt(process.env.PORT || '3000');
const httpApp = (0, express_1.default)();
const server = https_1.default.createServer({
    cert: fs_1.default.readFileSync(path_1.default.join(__dirname, '../certs/cert.pem')),
    key: fs_1.default.readFileSync(path_1.default.join(__dirname, '../certs/key.pem')),
}, httpApp);
exports.wsApp = new socket_io_1.default.Server(server);
httpApp.use(express_1.default.json());
httpApp.use((0, cors_1.default)({ maxAge: 3600 }));
httpApp.get('/', (req, res) => res.redirect('/ui-moche'));
httpApp.use('/ui-moche', express_1.default.static(path_1.default.join(__dirname, '../static')));
httpApp.use((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const username = req.header('x-client-username');
    const roomId = req.header('x-client-room');
    if (!username || !roomId)
        return next();
    const room = yield mediasoup_1.default.getRoom(roomId);
    const user = room.getUser(username);
    req.room = room;
    req.user = user;
    console.info(`[${req.method}]`, req.url, username);
    return next();
}));
httpApp.use((0, router_1.default)());
httpApp.use((0, errorHandlers_1.default)());
exports.wsApp.on('connection', (socket) => __awaiter(void 0, void 0, void 0, function* () {
    const { roomId, username } = socket.handshake.query;
    const room = yield mediasoup_1.default.getRoom(roomId.toString());
    room.getUser(username.toString()).socketId = socket.id;
    socket.on('disconnect', () => {
        room.delUser(username.toString());
    });
}));
mediasoup_1.default.init().then(() => {
    server.listen(PORT, () => {
        console.info(`Server started on port ${PORT}`);
    });
});
