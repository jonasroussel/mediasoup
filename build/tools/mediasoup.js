"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Room = exports.User = exports.Consumer = exports.Producer = void 0;
const os_1 = require("os");
const mediasoup_1 = require("mediasoup");
const main_1 = require("~/main");
class Producer {
    constructor(transport) {
        this.transport = transport;
    }
    close() {
        this.producer.close();
        this.transport.close();
    }
    mute() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.producer.pause();
        });
    }
    unmute() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.producer.resume();
        });
    }
}
exports.Producer = Producer;
class Consumer {
    constructor(transport) {
        this.transport = transport;
    }
    close() {
        this.consumer.close();
        this.transport.close();
    }
    mute() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.consumer.pause();
        });
    }
    unmute() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.consumer.resume();
        });
    }
}
exports.Consumer = Consumer;
class User {
    constructor(username) {
        this.consumers = {};
        this.username = username;
    }
    toJSON() {
        return {
            producer_id: this.producer.producer.id,
            username: this.username,
        };
    }
}
exports.User = User;
class Room {
    constructor(id, router) {
        this.users = {};
        this.id = id;
        this.router = router;
    }
    addUser(user) {
        this.users[user.username] = user;
        console.info(`"${user.username}" connected in room "${this.id}"`);
    }
    getUser(username) {
        return this.users[username];
    }
    delUser(username) {
        const user = this.users[username];
        const producerId = user.producer.producer.id;
        user.producer.close();
        for (let consumerId in user.consumers) {
            user.consumers[consumerId].close();
        }
        delete this.users[username];
        main_1.wsApp.emit(`${this.id}-user-quit`, { producer_id: producerId });
        console.info(`"${username}" disconnected from room "${this.id}"`);
    }
}
exports.Room = Room;
const codecs = [
    {
        kind: 'audio',
        mimeType: 'audio/opus',
        clockRate: 48000,
        channels: 2,
    },
];
const workers = [];
const rooms = {};
const _pickWorker = () => {
    const idx = Math.floor(Math.random() * workers.length);
    return workers[idx];
};
const init = () => __awaiter(void 0, void 0, void 0, function* () {
    for (let i = 0; i < (0, os_1.cpus)().length; i++) {
        workers.push(yield (0, mediasoup_1.createWorker)({
            rtcMinPort: 21000,
            rtcMaxPort: 42000,
        }));
    }
});
const getRoom = (id) => __awaiter(void 0, void 0, void 0, function* () {
    if (rooms[id] != undefined)
        return rooms[id];
    const worker = _pickWorker();
    const router = yield worker.createRouter({ mediaCodecs: codecs });
    const room = new Room(id, router);
    rooms[id] = room;
    return room;
});
exports.default = {
    init,
    getRoom,
};
