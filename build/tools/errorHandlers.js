"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.unknown = exports.path = exports.json = void 0;
const json = (error, req, res, next) => {
    if (!(error instanceof SyntaxError))
        return next(error);
    return res.status(400).json({
        error: {
            status: 400,
            type: 'invalid_json',
            message: error.message
        }
    });
};
exports.json = json;
const path = (req, res) => {
    return res.status(404).json({
        error: {
            status: 404,
            type: 'invalid_path',
            message: `${req.path} is not a valid path`
        }
    });
};
exports.path = path;
const unknown = (error, req, res, next) => {
    return res.status(500).json({
        error: {
            status: 500,
            type: 'unknown',
            message: 'An unknown error has occurred during the request'
        }
    });
};
exports.unknown = unknown;
exports.default = () => [exports.json, exports.path, exports.unknown];
