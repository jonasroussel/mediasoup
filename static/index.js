const mediasoup = require('mediasoup-client')

/**
 * @type string
 */
let username
/**
 * @type string
 */
let roomId

/**
 * @type mediasoup.Device
 */
let device

/**
 * @type mediasoup.types.Transport
 */
let sendTransport
/**
 * @type mediasoup.types.Producer
 */
let producer

/**
 * @type {{ [key: string]: mediasoup.types.Transport }}
 */
let recvTransports = {}
/**
 * @type {{ [key: string]: mediasoup.types.Consumer }}
 */
let consumers = {}

const request = async (url, body) => {
	const res = await fetch(url, {
		method: 'POST',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
			'X-Client-Username': username,
			'X-Client-Room': roomId,
		},
		body: JSON.stringify(body),
	})
	if (res.status >= 400) throw new Error()
	return await res.json()
}

const joinRoom = async () => {
	// Validations
	if (nameInput.value.trim().length < 1) {
		alert('username required')
		return
	}
	if (roomInput.value.trim().length < 1) {
		alert('room required')
		return
	}

	// Join room
	username = nameInput.value.trim()
	roomId = roomInput.value.trim()
	let rtpCapabilities
	let users
	try {
		const infos = await request('/room/join')
		rtpCapabilities = infos.rtp_capabilities
		users = infos.users
	} catch (ex) {
		alert('Username already used')
		return
	}

	// Create device
	device = new mediasoup.Device()
	await device.load({ routerRtpCapabilities: rtpCapabilities })

	// Get audio
	const stream = await navigator.mediaDevices.getUserMedia({
		video: false,
		audio: true,
	})
	const track = stream.getAudioTracks()[0]

	// Produce
	await createProducer(track)

	// Connect with websocket
	subscribeToRoom()

	// Change menu to room
	changeMenu()

	// Load current users
	for (let user of users) {
		const consumer = await createConsumer(user.producer_id)
		addUser(user.username, user.producer_id, consumer)
	}
}

const changeMenu = () => {
	// Hide login menu
	loginMenu.style = 'display: none'

	// Show room menu
	roomMenu.style = ''

	// Add user in the menu
	{
		const title = document.createElement('h1')
		title.style = 'display: flex; justify-content: center'
		title.innerHTML = `<p>Room: <b>${roomId}</b></p>`
		roomMenu.appendChild(title)
		const div = document.createElement('div')
		div.style = 'display: flex; align-items: center'
		div.innerHTML = `<h2>${nameInput.value.trim()} (Toi)</h2> <button id="mute-${producer.id}">Mute</button>`
		roomMenu.appendChild(div)
		document.getElementById(`mute-${producer.id}`).addEventListener('click', async () => {
			const button = document.getElementById(`mute-${producer.id}`)
			if (button.innerText === 'Mute') {
				await request('/mute', {
					mute: true,
					producer_id: producer.id,
				})
				button.innerText = 'Unmute'
			} else {
				await request('/mute', {
					mute: false,
					producer_id: producer.id,
				})
				button.innerText = 'Mute'
			}
		})
	}
}

const addUser = (username, producerId, consumer) => {
	const div = document.createElement('div')
	div.style = 'display: flex; align-items: center'
	div.id = producerId
	div.innerHTML = `
		<h2>${username}</h2>
		<button id="mute-${producerId}">Mute</button>
		<audio id="audio-${producerId}" autoplay controls></audio>
	`
	roomMenu.appendChild(div)
	document.getElementById(`mute-${producerId}`).addEventListener('click', async () => {
		const button = document.getElementById(`mute-${producerId}`)
		if (button.innerText === 'Mute') {
			await request('/mute', {
				mute: true,
				producer_id: producerId,
			})
			button.innerText = 'Unmute'
		} else {
			await request('/mute', {
				mute: false,
				producer_id: producerId,
			})
			button.innerText = 'Mute'
		}
	})
	document.getElementById(`audio-${producerId}`).srcObject = new MediaStream([consumer.track])
}

const delUser = (producerId) => {
	roomMenu.removeChild(document.getElementById(producerId))
	recvTransports[producerId].close()
	consumers[producerId].close()
	delete recvTransports[producerId]
	delete consumers[producerId]
}

const createProducer = (track) => {
	return new Promise(async (resolve, reject) => {
		const { transport } = await request('/producer/add', {})
		sendTransport = device.createSendTransport({
			id: transport.id,
			iceParameters: transport.ice_parameters,
			iceCandidates: transport.ice_candidates,
			dtlsParameters: transport.dtls_parameters,
		})

		sendTransport.on('connect', async ({ dtlsParameters }, success, error) => {
			try {
				await request('/producer/connect', {
					dtls_parameters: dtlsParameters,
				})

				success()
			} catch (ex) {
				error()
			}
		})
		sendTransport.on('produce', async ({ kind, rtpParameters }, success, error) => {
			try {
				const { producer_id } = await request('/producer/produce', {
					kind: kind,
					rtp_parameters: rtpParameters,
				})

				success({ id: producer_id })
			} catch (ex) {
				error()
			}
		})

		producer = await sendTransport.produce({ track })

		resolve()
	})
}

const createConsumer = async (producerId) => {
	return new Promise(async (resolve, reject) => {
		const { transport } = await request('/consumer/add', { producer_id: producerId })
		recvTransports[producerId] = device.createRecvTransport({
			id: transport.id,
			iceParameters: transport.ice_parameters,
			iceCandidates: transport.ice_candidates,
			dtlsParameters: transport.dtls_parameters,
		})

		recvTransports[producerId].on('connect', async ({ dtlsParameters }, success, error) => {
			try {
				await request('/consumer/connect', {
					dtls_parameters: dtlsParameters,
					producer_id: producerId,
				})

				success()
			} catch (ex) {
				error()
			}
		})

		const consumer = await request('/consumer/consume', {
			rtp_capabilities: device.rtpCapabilities,
			producer_id: producerId,
		})

		consumers[producerId] = await recvTransports[producerId].consume({
			id: consumer.consumer_id,
			producerId: consumer.producer_id,
			kind: consumer.kind,
			rtpParameters: consumer.rtp_parameters,
		})

		resolve(consumers[producerId])
	})
}

const subscribeToRoom = () => {
	const socket = require('socket.io-client').connect({
		query: `roomId=${encodeURIComponent(roomId)}&username=${encodeURIComponent(username)}`,
		reconnection: false,
	})

	socket.on(`${roomId}-user-join`, async ({ username, producer_id }) => {
		const consumer = await createConsumer(producer_id)
		addUser(username, producer_id, consumer)
	})

	socket.on(`${roomId}-user-quit`, ({ producer_id }) => {
		delUser(producer_id)
	})
}

joinRoomButton.addEventListener('click', joinRoom)
