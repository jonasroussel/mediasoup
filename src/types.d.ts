import { Room, User } from '~/tools/mediasoup'

declare global {
	namespace Express {
		interface Request {
			user?: User
			room?: Room
		}
	}
}

