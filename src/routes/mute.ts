import { Router } from 'express'

const route = Router()

route.post('/', async (req, res) => {
	const mute = req.body.mute || false
	if (req.user.producer.producer.id === req.body.producer_id) {
		const producer = req.user.producer
		if (mute) {
			await producer.mute()
		} else {
			await producer.unmute()
		}
	} else {
		const consumer = req.user.consumers[req.body.producer_id]
		if (mute) {
			await consumer.mute()
		} else {
			await consumer.unmute()
		}
	}
	
	return res.status(200).json({})
})

export default route
