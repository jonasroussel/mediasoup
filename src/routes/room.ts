import { Router } from 'express'

import { User } from '~/tools/mediasoup'

const route = Router()

route.post('/join', async (req, res) => {
	if (req.user != null) return res.status(400).json({})
	
	const users = Object.values(req.room.users)

	req.room.addUser(new User(req.header('x-client-username')))

	return res.status(200).json({
		rtp_capabilities: req.room.router.rtpCapabilities,
		users: users.map((u) => u.toJSON()),
	})
})

export default route
