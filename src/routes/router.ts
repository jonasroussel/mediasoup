import { Router } from 'express'

import room from './room'
import produce from './producer'
import consumer from './consumer'
import mute from './mute'

export default () => {
	const router = Router({ mergeParams: true })

	router.use('/room', room)
	router.use('/producer', produce)
	router.use('/consumer', consumer)
	router.use('/mute', mute)

	return router
}
