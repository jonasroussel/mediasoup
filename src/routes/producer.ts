import { Router } from 'express'

import { wsApp } from '~/main'
import { Producer } from '~/tools/mediasoup'

const route = Router()

route.post('/add', async (req, res) => {
	const transport = await req.room.router.createWebRtcTransport({
		listenIps: [{ ip: process.env.HOST }],
		enableTcp: true,
		enableUdp: true,
		preferUdp: true,
	})

	req.user.producer = new Producer(transport)

	return res.status(200).json({
		transport: {
			id: transport.id,
			ice_parameters: transport.iceParameters,
			ice_candidates: transport.iceCandidates,
			dtls_parameters: transport.dtlsParameters,
		},
	})
})

route.post('/connect', async (req, res) => {
	await req.user.producer.transport.connect({ dtlsParameters: req.body.dtls_parameters })

	return res.status(200).json({})
})

route.post('/produce', async (req, res) => {
	const producer = await req.user.producer.transport.produce({
		kind: req.body.kind,
		rtpParameters: req.body.rtp_parameters,
	})
	req.user.producer.producer = producer

	wsApp.emit(`${req.room.id}-user-join`, {
		username: req.user.username,
		producer_id: producer.id,
	})

	return res.status(200).json({
		producer_id: producer.id,
	})
})

export default route
