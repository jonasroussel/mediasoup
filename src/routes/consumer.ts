import { Router } from 'express'

import { Consumer } from '~/tools/mediasoup'

const route = Router()

route.post('/add', async (req, res) => {
	const transport = await req.room.router.createWebRtcTransport({
		listenIps: [{ ip: process.env.HOST }],
		enableTcp: true,
		enableUdp: true,
		preferUdp: true,
	})

	req.user.consumers[req.body.producer_id] = new Consumer(transport)

	return res.status(200).json({
		transport: {
			id: transport.id,
			ice_parameters: transport.iceParameters,
			ice_candidates: transport.iceCandidates,
			dtls_parameters: transport.dtlsParameters,
		},
	})
})

route.post('/connect', async (req, res) => {
	req.user.consumers[req.body.producer_id].transport.connect({ dtlsParameters: req.body.dtls_parameters })

	return res.status(200).json({})
})

route.post('/consume', async (req, res) => {
	const transport = req.user.consumers[req.body.producer_id].transport
	const consumer = await transport.consume({
		producerId: req.body.producer_id,
		rtpCapabilities: req.body.rtp_capabilities,
	})
	
	req.user.consumers[req.body.producer_id].consumer = consumer

	return res.status(200).json({
		consumer_id: consumer.id,
		producer_id: consumer.producerId,
		kind: consumer.kind,
		rtp_parameters: consumer.rtpParameters,
	})
})

export default route
