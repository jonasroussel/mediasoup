import 'module-alias/register'
require('dotenv').config()
import '~/tools/logger'

import fs from 'fs'
import path from 'path'

import cors from 'cors'
import https from 'https'
import wsio from 'socket.io'
import express from 'express'

import router from '~/routes/router'
import mediasoup from './tools/mediasoup'
import errorHandlers from '~/tools/errorHandlers'

/******************/
/* Initialization */
/******************/

// Server Port
const PORT = parseInt(process.env.PORT || '3000')

// HTTP Application
const httpApp = express()

// HTTPS Server
const server = https.createServer(
	{
		cert: fs.readFileSync(path.join(__dirname, '../certs/cert.pem')),
		key: fs.readFileSync(path.join(__dirname, '../certs/key.pem')),
	},
	httpApp
)

// WS Application
export const wsApp = new wsio.Server(server)

/********************/
/* HTTP Application */
/********************/

// JSON Parsing
httpApp.use(express.json())

// CORS Infos
httpApp.use(cors({ maxAge: 3600 }))

// Redirect
httpApp.get('/', (req, res) => res.redirect('/ui-moche'))

// Static Content
httpApp.use('/ui-moche', express.static(path.join(__dirname, '../static')))

// Middleware
httpApp.use(async (req, res, next) => {
	const username = req.header('x-client-username')
	const roomId = req.header('x-client-room')
	if (!username || !roomId) return next()

	const room = await mediasoup.getRoom(roomId)
	const user = room.getUser(username)

	req.room = room
	req.user = user

	console.info(`[${req.method}]`, req.url, username)

	return next()
})

// Routes
httpApp.use(router())

// Error Handlers
httpApp.use(errorHandlers())

/*************************/
/* Websocket Application */
/*************************/

wsApp.on('connection', async (socket) => {
	const { roomId, username } = socket.handshake.query

	const room = await mediasoup.getRoom(roomId.toString())
	room.getUser(username.toString()).socketId = socket.id

	socket.on('disconnect', () => {
		room.delUser(username.toString())
	})
})

/******************/
/* Start Services */
/******************/

mediasoup.init().then(() => {
	server.listen(PORT, () => {
		console.info(`Server started on port ${PORT}`)
	})
})
