import { cpus } from 'os'
import { createWorker, types } from 'mediasoup'

import { wsApp } from '~/main'

/**********/
/* MODELS */
/**********/

export class Producer {
	public transport: types.WebRtcTransport
	public producer: types.Producer

	constructor(transport: types.WebRtcTransport) {
		this.transport = transport
	}

	public close() {
		this.producer.close()
		this.transport.close()
	}

	public async mute() {
		await this.producer.pause()
	}
	public async unmute() {
		await this.producer.resume()
	}
}

export class Consumer {
	public transport: types.WebRtcTransport
	public consumer: types.Consumer

	constructor(transport: types.WebRtcTransport) {
		this.transport = transport
	}

	public close() {
		this.consumer.close()
		this.transport.close()
	}

	public async mute() {
		await this.consumer.pause()
	}
	public async unmute() {
		await this.consumer.resume()
	}
}

export class User {
	public readonly username: string

	public socketId: string
	public producer?: Producer
	public consumers: { [key: string]: Consumer } = {}

	constructor(username: string) {
		this.username = username
	}

	public toJSON() {
		return {
			producer_id: this.producer.producer.id,
			username: this.username,
		}
	}
}

export class Room {
	public readonly id: string
	public readonly router: types.Router

	public users: { [key: string]: User } = {}

	constructor(id: string, router: types.Router) {
		this.id = id
		this.router = router
	}

	public addUser(user: User) {
		this.users[user.username] = user
		console.info(`"${user.username}" connected in room "${this.id}"`)
	}
	public getUser(username: string) {
		return this.users[username]
	}
	public delUser(username: string) {
		const user = this.users[username]
		const producerId = user.producer.producer.id

		user.producer.close()
		for (let consumerId in user.consumers) {
			user.consumers[consumerId].close()
		}
		delete this.users[username]

		wsApp.emit(`${this.id}-user-quit`, { producer_id: producerId })

		console.info(`"${username}" disconnected from room "${this.id}"`)
	}
}

/*******************/
/* CONSTS & STATES */
/*******************/

const codecs: types.RtpCodecCapability[] = [
	{
		kind: 'audio',
		mimeType: 'audio/opus',
		clockRate: 48000,
		channels: 2,
	},
]

const workers: types.Worker[] = []
const rooms: { [key: string]: Room } = {}

/******************/
/* Private Method */
/******************/

const _pickWorker = () => {
	const idx = Math.floor(Math.random() * workers.length)
	return workers[idx]
}

/******************/
/* Public Method */
/******************/

const init = async () => {
	for (let i = 0; i < cpus().length; i++) {
		workers.push(
			await createWorker({
				rtcMinPort: 21000,
				rtcMaxPort: 42000,
			})
		)
	}
}

const getRoom = async (id: string): Promise<Room> => {
	if (rooms[id] != undefined) return rooms[id]

	const worker = _pickWorker()
	const router = await worker.createRouter({ mediaCodecs: codecs })
	const room = new Room(id, router)

	rooms[id] = room

	return room
}

// const createTransport = async (room: Room): Promise<Transport> => {
// 	const transport = await room.router.createWebRtcTransport({
// 		listenIps: [{ ip: '192.168.0.42' }],
// 		enableUdp: true,
// 		enableTcp: true,
// 		preferUdp: true,
// 	})

// 	transports[room.id].push(transport)

// 	return transport
// }

// const getTransport = (room: Room, id: string): Transport => {
// 	return transports[room.id].find((t) => t.id === id)
// }

// const connectTransport = async (transport: Transport, dtlsParameters: types.DtlsParameters): Promise<void> => {
// 	await transport.connect({ dtlsParameters })
// }

// const getProducers = (room: Room): Producer[] => producers[room.id]

// const createProducer = async (
// 	room: Room,
// 	transport: Transport,
// 	kind: types.MediaKind,
// 	rtpParameters: types.RtpParameters
// ): Promise<Producer> => {
// 	const producer = await transport.produce({
// 		kind: kind,
// 		rtpParameters: rtpParameters,
// 	})

// 	producers[room.id].push(producer)

// 	return producer
// }

// const createConsumer = async (
// 	room: Room,
// 	transport: Transport,
// 	producerId: string,
// 	rtpCapabilities: types.RtpCapabilities
// ): Promise<Consumer> => {
// 	const consumer = await transport.consume({
// 		producerId,
// 		rtpCapabilities,
// 	})

// 	consumers[room.id].push(consumer)

// 	return consumer
// }

/***********/
/* Exports */
/***********/

export default {
	init,
	getRoom,
}
