import { Request, Response, NextFunction } from 'express';

// JSON Parsing Error
export const json = (error: any, req: Request, res: Response, next: NextFunction) => {
	if (!(error instanceof SyntaxError)) return next(error);

	return res.status(400).json({
		error: {
			status: 400,
			type: 'invalid_json',
			message: error.message
		}
	});
};

// Invalid Path Error
export const path = (req: Request, res: Response) => {
	return res.status(404).json({
		error: {
			status: 404,
			type: 'invalid_path',
			message: `${req.path} is not a valid path`
		}
	});
};

// Unknown Error
export const unknown = (error: any, req: Request, res: Response, next: NextFunction) => {
	return res.status(500).json({
		error: {
			status: 500,
			type: 'unknown',
			message: 'An unknown error has occurred during the request'
		}
	});
};

export default () => [json, path, unknown];
