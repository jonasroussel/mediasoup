const format = (date: Date) =>
	date
		.toISOString()
		.replace('T', ' ')
		.replace(/\.[0-9]*Z/, '');

console.info = (message?: any, ...optionalParams: any[]) => {
	console.log(`\x1B[96m[${format(new Date())}] [INFO] ${message}`, ...optionalParams, '\x1B[0m');
};

console.warn = (message?: any, ...optionalParams: any[]) => {
	console.log(`\x1B[33m[${format(new Date())}] [WARNING] ${message}`, ...optionalParams, '\x1B[0m');
};

console.error = (message?: any, ...optionalParams: any[]) => {
	console.log(`\x1B[91m[${format(new Date())}] [ERROR] ${message}`, ...optionalParams, '\x1B[0m');
};
